﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum TTileState
{
    kAll = -1,
    kBlocked = 1,
    kPassage = 0
}
public class Tiles
{
    public Tiles(int row,int col,TTileState state = TTileState.kBlocked)
    {
        m_row   = row;
        m_col   = col;
        m_state = state;
    }

    
    public TTileState State { get { return m_state; } set { m_state = value; } }
    public int Row { get { return m_row; } }
    public int Col { get { return m_col; } }

    private int m_row;
    private int m_col;
    private TTileState m_state;
}


public class WorldGeneration : MonoBehaviour
{
    public int m_numRows = 10;
    public int m_numCols = 10;

    public float m_width = 1.0f;

    [Range(0,0.5f)]
    public float m_percentBlocked = 0.0f;

    //wall prefab
    public GameObject m_walls;

    public GameObject m_floors;

    public GameObject m_parent;

    public Vector3 m_startPos;

    private Tiles[][] m_tileArray;

    private List<Tiles> m_tileNeighs;

    private void Start()
    {
        CreateGrid();
    }

    void CreateGrid()
    {
        InitializeTiles();
        PrimsAlgorithm();
        MakeBorders();
        MakeMoreWalls();
        DrawMaze();
    }
    void MakeMoreWalls()
    {
        int totalTiles = m_numCols * m_numRows;
        totalTiles = (int)(totalTiles * m_percentBlocked);
        int count = 0;
        while ( count < totalTiles)
        {
            int randX = Random.Range(0, m_numRows);
            int randY = Random.Range(0, m_numCols);
            Tiles randTile = m_tileArray[randX][randY];
            randTile.State = TTileState.kBlocked;
            count++;
        }
    }

    void DrawMaze()
    {
        for (int row = 0; row < m_numRows; row++)
        {
            for (int col = 0; col < m_numCols; col++)
            {
                if (m_tileArray[row][col].State == TTileState.kBlocked)
                {
                    SpawnGridTile(m_walls, row, col);
                }
                else
                {
                    SpawnGridTile(m_floors, row, col);
                }
            }
        }
    }
    void InitializeTiles()
    {
        m_tileArray = new Tiles[m_numRows][]; // intialize the row set
        for (int row = 0; row < m_numRows; row++)
        {
            m_tileArray[row] = new Tiles[m_numCols]; // initialize the col set for each row
            for (int col = 0; col < m_numCols; col++)
            {
                m_tileArray[row][col] = new Tiles(row, col, TTileState.kBlocked);
            }
        }
        m_tileNeighs = new List<Tiles>();
    }
    void MakeBorders()
    {
        //Keep the borders as blockable
        for (int row = 0; row < m_numRows; row += (m_numRows - 1))
        {
            for (int col = 0; col < m_numCols; col++)
            {
                Debug.Log(" " + row + " " + col);
                m_tileArray[row][col].State = TTileState.kBlocked;
            }
        }
        for (int col = 0; col < m_numCols; col += (m_numCols - 1))
        {
            for (int row = 0; row < m_numRows; row++)
            {
                Debug.Log(" " + row + " " + col);
                m_tileArray[row][col].State = TTileState.kBlocked;
            }
        }
    }

    void SpawnGridTile(GameObject prefab,int row, int col)
    {
        GameObject obj = GameObject.Instantiate(prefab);
        obj.transform.position = new Vector3(m_startPos.x + row * m_width,
                    m_startPos.y,
                    m_startPos.z + col * m_width);
        if (m_parent != null)
        {
            obj.transform.SetParent(m_parent.transform);
        }
    }

    void PrimsAlgorithm()
    {
        int startX = Random.Range(0, m_numRows);
        int startY = Random.Range(0, m_numCols);
        Tiles startTile = m_tileArray[startX][startY];
        startTile.State = TTileState.kPassage;
        GetNeighbourTiles(m_tileNeighs, startTile, TTileState.kBlocked);
        while(m_tileNeighs.Count > 0 )
        {
            Tiles curr = m_tileNeighs[Random.Range(0,m_tileNeighs.Count)];            
            List<Tiles> frontierNeigh = new List<Tiles>();
            GetNeighbourTiles(frontierNeigh, curr, TTileState.kAll);
            if(frontierNeigh.Count > 0)
            {
                curr.State = TTileState.kPassage;
                Tiles randCell = frontierNeigh[Random.Range(0, frontierNeigh.Count)];
                int inBtCellRow = curr.Row + ((randCell.Row - curr.Row) / 2);
                int inBtCellCol = curr.Col + ((randCell.Col - curr.Col) / 2);
                m_tileArray[inBtCellRow][inBtCellCol].State = TTileState.kPassage;
            }                       
            m_tileNeighs.Remove(curr);
            GetNeighbourTiles(m_tileNeighs, curr, TTileState.kBlocked);
        }
    }

    void GetNeighbourTiles(List<Tiles> tileList, Tiles tile, TTileState state)
    {
        List<Tiles> neighbours = new List<Tiles>();

        //top
        int colTop = tile.Col + 2;
        if (colTop >= 0 && colTop < m_numCols)
        {
            Tiles obj = m_tileArray[tile.Row][colTop];
            if (obj.State == state || state == TTileState.kAll)
            {
                //this is a neighbour
                if (!tileList.Contains(obj))
                {
                    tileList.Add(obj);
                }
            }
        }
        //bottom
        int colBot = tile.Col - 2;
        if (colBot >= 0 && colBot < m_numCols)
        {
            Tiles obj = m_tileArray[tile.Row][colBot];
            if (obj.State == state || state == TTileState.kAll)
            {
                //this is a neighbour
                if (!tileList.Contains(obj))
                {
                    tileList.Add(obj);
                }
            }
        }
        //left
        int rowLeft = tile.Row + 2;
        if (rowLeft >= 0 && rowLeft < m_numRows)
        {
            Tiles obj = m_tileArray[rowLeft][tile.Col];
            if (obj.State == state || state == TTileState.kAll)
            {
                //this is a neighbour
                if (!tileList.Contains(obj))
                {
                    tileList.Add(obj);
                }
            }
        }
        //right
        int rowRight = tile.Row - 2;
        if (rowRight >= 0 && rowRight < m_numRows)
        {
            Tiles obj = m_tileArray[rowRight][tile.Col];
            if (obj.State == state || state == TTileState.kAll)
            {
                //this is a neighbour
                if (!tileList.Contains(obj))
                {
                    tileList.Add(obj);
                }
            }
        }
    }
}
