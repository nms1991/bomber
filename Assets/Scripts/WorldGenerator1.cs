﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WorldGenerator1 : MonoBehaviour
{
    public int m_numRows    = 10;
    public int m_numCols    = 10;

    public int m_width  = 1;
    public int m_height = 1;

    public GameObject m_floorPrefab;
    public GameObject m_wallPrefab;

    public GameObject m_parent = null;

    private GameObject[][]  m_gridGameObjs;
    private int[][]         m_gridMatrix;         
    private Vector3         m_center = new Vector3(0,0,0);


    private void Start()
    {
        GenerateGrid();
    }

    List<CellData> m_frontierList;
    struct CellData
    {
        public CellData(int row,int col)
        {
            m_col = col;
            m_row = row;
        }
        public int m_row;
        public int m_col;
    }

    [ContextMenu("Generate")]
    void GenerateGrid()
    {
        Debug.Assert(m_floorPrefab != null && m_wallPrefab != null, "Make sure the floor and wall prefabs are assigned");
        m_gridGameObjs = new GameObject[m_numRows][];
        m_gridMatrix = new int[m_numRows][];
        for (int row = 0; row < m_numRows; row++)
        {
            m_gridGameObjs[row] = new GameObject[m_numCols];
            m_gridMatrix[row] = new int[m_numCols];
            for (int col = 0; col < m_numCols; col++)
            {
                m_gridMatrix[row][col] = 1;
            }
        }

        Prims();

        for (int row = 0; row < m_numRows; row++)
        {
            for (int col = 0; col < m_numCols; col++)
            {
                if(m_gridMatrix[row][col] == 0)
                {
                    GameObject obj = GameObject.Instantiate(m_floorPrefab);
                    m_gridGameObjs[row][col] = obj;
                    if (m_parent)
                    {
                        obj.transform.SetParent(m_parent.transform);
                    }
                    obj.transform.position = new Vector3(m_center.x + row * m_width, m_center.y, m_center.z + col * m_height);
                }
                else
                {
                    GameObject obj = GameObject.Instantiate(m_wallPrefab);
                    m_gridGameObjs[row][col] = obj;
                    if (m_parent)
                    {
                        obj.transform.SetParent(m_parent.transform);
                    }
                    obj.transform.position = new Vector3(m_center.x + row * m_width, m_center.y, m_center.z + col * m_height);
                }
            }
        }
    }

    void Prims()
    {
        m_frontierList = new List<CellData>();
        int startRow    = Random.Range(0, m_numRows-1);
        int startCol     = Random.Range(0, m_numCols-1);
        m_gridMatrix[startRow][startCol]= 0;
        AddNeighbourCells(startRow, startCol,m_frontierList,true);
        while(m_frontierList.Count > 0)
        {
            List<CellData> neighs = new List<CellData>();
            int fRow = m_frontierList[0].m_row;
            int fCol = m_frontierList[0].m_col;
            AddNeighbourCells(fRow, fCol, neighs);
            if(neighs.Count > 0 )
            {
                CellData randCell = neighs[Random.Range(0, neighs.Count)];
                m_gridMatrix[fRow][fCol] = 0;
                int inBtCellRow = fRow + ((randCell.m_row - fRow) / 2);
                int inBtCellCol = fCol + ((randCell.m_col - fCol) / 2);
                if (isValid(inBtCellRow, inBtCellCol))
                {
                    Debug.Log(fRow + " " + fCol + " " + randCell.m_row + " " + randCell.m_col + " " + inBtCellRow + " " + inBtCellCol);
                    m_gridMatrix[inBtCellRow][inBtCellCol] = 0;
                }
            }
            m_frontierList.Remove(m_frontierList[0]);
            AddNeighbourCells(fRow, fCol, m_frontierList, true);
        }
    }

    bool isValid(int row,int col)
    {
        if(row >= 0 && row < m_numRows && col >=0 && col < m_numCols)
        {
            return true;
        }
        return false;
    }

    void AddNeighbourCells(int row,int col,List<CellData> data,bool isFrontier = false)
    {
        if(!isValid(row,col))
        {
            return;
        }
        //left
        int leftRow = row - 2;
        if(leftRow >= 0 && leftRow < m_numRows)
        {
            if (m_gridMatrix[leftRow][col] == 1 || !isFrontier)
            {
                CellData objData = new CellData(leftRow, col);
                if (!data.Contains(objData))
                    data.Add(objData);
            }
        }
        //right
        int rightRow = row + 2;
        if(rightRow >= 0 && rightRow < m_numRows)
        {
            if (m_gridMatrix[rightRow][col] == 1 || !isFrontier)
            {
                CellData objData = new CellData(rightRow, col);
                if (!data.Contains(objData))
                    data.Add(objData);
            }
        }
        //top 
        int topCol = col - 2;
        if(topCol >= 0 && topCol < m_numCols)
        {
            Debug.Log("top" + topCol);
            if (m_gridMatrix[row][topCol] == 1 || !isFrontier)
            {
                CellData objData = new CellData(row, topCol);
                if (!data.Contains(objData))
                    data.Add(objData);
            }
        }
        //bottom
        int botCol = col + 2;
        if (botCol >= 0 && botCol < m_numCols)
        {
            Debug.Log("bot" + botCol);
            if (m_gridMatrix[row][botCol] == 1 || !isFrontier)
            {
                CellData objData = new CellData(row, botCol);
                if (!data.Contains(objData))
                    data.Add(objData);
            }
        }
    }
}
